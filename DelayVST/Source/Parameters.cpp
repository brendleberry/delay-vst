#include "Parameters.h"

namespace
{
    const float min_freq_hz = 20.f;
	const float max_freq_hz = 20000.f;
	const float freq_interval_value = 0.1f;
	const float freq_skew_factor = 0.15f;
	const auto filter_freq_range = juce::NormalisableRange<float>(min_freq_hz, max_freq_hz, freq_interval_value, freq_skew_factor);

	//const float min_gain_db = -24.f;
	//const float max_gain_db = 24.f;
	//const float gain_interval_value = 0.1f;
	//const float gain_skew_factor = 1.f;
	//const auto peak_filter_gain_range = juce::NormalisableRange<float>(min_gain_db, max_gain_db, gain_interval_value, gain_skew_factor);

	//const float min_quality = 0.1f;
	//const float max_quality = 10.f;
	//const float quality_interval_value = 0.05f;
	//const float quality_skew_factor = 0.5f;
	//const auto peak_filter_quality_range = juce::NormalisableRange<float>(min_quality, max_quality, quality_interval_value, quality_skew_factor);
}

namespace DelayVST
{
	// parameters for the mid chain
	const NamedParameter MidChainParameters::lowpass_freq = { "Mid Lowpass Freq", 20000.f, filter_freq_range };
	const NamedParameter MidChainParameters::highpass_freq = { "Mid Highpass Freq", 20.f, filter_freq_range };

	// parameters for the side chain
	const NamedParameter SideChainParameters::lowpass_freq = { "Side Lowpass Freq", 20000.f, filter_freq_range };
	const NamedParameter SideChainParameters::highpass_freq = { "Side Highpass Freq", 20.f, filter_freq_range };

	//const NamedParameter peak_1_freq = { "Peak 1 Freq", 250.f };
	//const NamedParameter peak_1_gain = { "Peak 1 Gain", 0.f };
	//const NamedParameter peak_1_quality = { "Peak 1 Quality", 1.f };

	//const NamedParameter peak_2_freq = { "Peak 2 Freq", 1000.f };
	//const NamedParameter peak_2_gain = { "Peak 2 Gain", 0.f };
	//const NamedParameter peak_2_quality = { "Peak 2 Quality", 1.f };

	//const NamedParameter peak_3_freq = { "Peak 3 Freq", 4000.f };
	//const NamedParameter peak_3_gain = { "Peak 3 Gain", 0.f };
	//const NamedParameter peak_3_quality = { "Peak 3 Quality", 1.f };
}
