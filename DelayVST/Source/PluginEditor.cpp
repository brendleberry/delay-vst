/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"


DelayVSTAudioProcessorEditor::DelayVSTAudioProcessorEditor (DelayVSTAudioProcessor& p)
    : AudioProcessorEditor (&p), audioProcessor (p)
{
    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.
    setSize (400, 300);

    // add components
    addComponents();
}

DelayVSTAudioProcessorEditor::~DelayVSTAudioProcessorEditor()
{
}

void DelayVSTAudioProcessorEditor::paint (juce::Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel().findColour (juce::ResizableWindow::backgroundColourId));

    //g.setColour (juce::Colours::white);
    //g.setFont (15.0f);
    //g.drawFittedText ("DelayVST", getLocalBounds(), juce::Justification::topLeft, 1);
}

void DelayVSTAudioProcessorEditor::resized()
{
    // This is generally where you'll want to lay out the positions of any
    // subcomponents in your editor..

    //// set the position and size of the gain slider
    //_gain_slider.setBounds(getLocalBounds());

    //// set the position and size of the delay slider
    //_delay_slider.setBounds(getLocalBounds());
}

void DelayVSTAudioProcessorEditor::sliderValueChanged(juce::Slider* slider)
{
    // pass the new gain value onto the audio processor
    //audioProcessor.gain(_gain_slider.getValue());

    // pass the new delay amount onto the audio processor
    //audioProcessor.delayAmountS(_delay_slider.getValue());
}

void DelayVSTAudioProcessorEditor::addComponents()
{
    //// add the gain slider
    //_gain_slider.setSliderStyle(juce::Slider::SliderStyle::LinearVertical);
    //_gain_slider.setTextBoxStyle(juce::Slider::TextBoxBelow, false, TEXT_BOX_WIDTH_PX, TEXT_BOX_HEIGHT_PX);
    //_gain_slider.setRange(DEFAULT_RANGE, DEFAULT_RANGE_INCREMENT);

    //_gain_slider.addListener(this); // lets us actually do things with the slider value
    //addAndMakeVisible(_gain_slider);

    //// add the delay slider
    //_delay_slider.setSliderStyle(juce::Slider::SliderStyle::LinearVertical);
    //_delay_slider.setTextBoxStyle(juce::Slider::TextBoxBelow, false, TEXT_BOX_WIDTH_PX, TEXT_BOX_HEIGHT_PX);
    //_delay_slider.setRange(DEFAULT_RANGE, DEFAULT_RANGE_INCREMENT);

    //_delay_slider.addListener(this); // lets us actually do things with the slider value
    //addAndMakeVisible(_delay_slider);
}

