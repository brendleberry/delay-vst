#pragma once
#include <type_traits>
namespace Utils
{
	template<class T, class=typename std::enable_if<std::is_enum<T>::value>::type>
	std::underlying_type_t<T> toUnderlying(T& t)
	{
		return static_cast<std::underlying_type_t<T>>(t);
	}
}