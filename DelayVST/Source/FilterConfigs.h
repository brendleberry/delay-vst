#pragma once

namespace DelayVST
{
	// Generic filter config - all filter configs derive from this
	struct FilterConfig
	{
		virtual ~FilterConfig() {}
	};

	struct HighpassFilterConfig : public FilterConfig
	{
		float _highpass_freq_hz = 0.f;
		float _highpass_slope_db_per_octave = 12.f;
	};

	struct LowpassFilterConfig : public FilterConfig
	{
		float _lowpass_freq_hz = 0.f;
		float _lowpass_slope_db_per_octave = 12.f;
	};

	struct PeakFilterConfig : public FilterConfig
	{
		float _peak_freq_hz = 0.f;
		float _peak_gain_db = 0.f;
		float _peak_quality = 1.f;
	};
}