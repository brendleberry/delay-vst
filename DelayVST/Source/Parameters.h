#pragma once

#include <JuceHeader.h>
#include <memory>
#include <string>

namespace DelayVST
{
    struct NamedParameter
    {
        const std::string _name;
        const float _default;
        const juce::NormalisableRange<float> _range;

        void addToLayout(juce::AudioProcessorValueTreeState::ParameterLayout& layout) const 
        {
            layout.add(std::make_unique<juce::AudioParameterFloat>(_name, _name, _range, _default));
        }
    };

    // parameters for the mid chain
    struct MidChainParameters
    {
        static const NamedParameter lowpass_freq;
        static const NamedParameter highpass_freq;
    };

    // parameters for the side chain
    struct SideChainParameters
    {
        static const NamedParameter lowpass_freq;
        static const NamedParameter highpass_freq;
    };
}
