/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"
#include "Parameters.h"

namespace
{
    void MidSideEncode(juce::AudioBuffer<float>& buffer, int size)
    {
        // convert from L-R to mid-side
        float* left_channel = buffer.getWritePointer(0);
        float* right_channel = buffer.getWritePointer(1);

        for (int i = 0; i < size; i++)
        {
            float mid_sample = (left_channel[i] * 0.5f) + (right_channel[i] * 0.5f); // mid = (L+R)/2
            float side_sample = (left_channel[i] * 0.5f) - (right_channel[i] * 0.5f); // side = (L-R)/2

            left_channel[i] = mid_sample;
            right_channel[i] = side_sample;
        }
    }

    void LeftRightEncode(juce::AudioBuffer<float>& buffer, int size)
    {
        //  convert back from mid-side to L-R
        float* mid_channel = buffer.getWritePointer(0);
        float* side_channel = buffer.getWritePointer(1);

        for (int i = 0; i < size; i++)
        {
            // L = (L+R)/2 + (L-R)/2 = (mid + side)
            float left_sample = mid_channel[i] + side_channel[i];

            // R = (L+R)/2 - (L-R)/2 = (mid - side)
            float right_sample = mid_channel[i] - side_channel[i];

            mid_channel[i] = left_sample;
            side_channel[i] = right_sample;
        }
    }
}

DelayVSTAudioProcessor::DelayVSTAudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
    : AudioProcessor (BusesProperties() 
        #if ! JucePlugin_IsMidiEffect
        #if ! JucePlugin_IsSynth
        .withInput  ("Input",  juce::AudioChannelSet::stereo(), true)
        #endif
        .withOutput ("Output", juce::AudioChannelSet::stereo(), true)
        #endif
        )
#endif
{

}

DelayVSTAudioProcessor::~DelayVSTAudioProcessor()
{
}

const juce::String DelayVSTAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool DelayVSTAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool DelayVSTAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool DelayVSTAudioProcessor::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}

double DelayVSTAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int DelayVSTAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int DelayVSTAudioProcessor::getCurrentProgram()
{
    return 0;
}

void DelayVSTAudioProcessor::setCurrentProgram (int index)
{
}

const juce::String DelayVSTAudioProcessor::getProgramName (int index)
{
    return {};
}

void DelayVSTAudioProcessor::changeProgramName (int index, const juce::String& newName)
{
}

void DelayVSTAudioProcessor::prepareToPlay(double sampleRate, int samplesPerBlock)
{
    // Use this method as the place to do any pre-playback
    // initialisation that you need..
    juce::dsp::ProcessSpec spec;
    spec.maximumBlockSize = samplesPerBlock;
    spec.numChannels = getTotalNumOutputChannels();
    spec.sampleRate = sampleRate;

    _mid_chain.prepare(spec);
    _side_chain.prepare(spec);

    updateFilters();
}

void DelayVSTAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool DelayVSTAudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    juce::ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    // Some plugin hosts, such as certain GarageBand versions, will only
    // load plugins that support stereo bus layouts.
    if (layouts.getMainOutputChannelSet() != juce::AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != juce::AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif

void DelayVSTAudioProcessor::processBlock (juce::AudioBuffer<float>& buffer, juce::MidiBuffer& midiMessages)
{
    juce::ScopedNoDenormals no_denormals;

    const int total_num_input_channels = getTotalNumInputChannels();
    const int total_num_output_channels = getTotalNumOutputChannels();
    const int num_samples = buffer.getNumSamples();

    // In case we have more outputs than inputs, this code clears any output
    // channels that didn't contain input data, (because these aren't
    // guaranteed to be empty - they may contain garbage).
    // This is here to avoid people getting screaming feedback
    // when they first compile a plugin, but obviously you don't need to keep
    // this code if your algorithm always overwrites all the output channels.
    for (int i = total_num_input_channels; i < total_num_output_channels; ++i)
    {
        buffer.clear(i, 0, buffer.getNumSamples());
    }

    updateFilters();
    MidSideEncode(buffer, num_samples);

    // process the buffer
    juce::dsp::AudioBlock<float> block(buffer);
    auto mid_block = block.getSingleChannelBlock(0);
    auto side_block = block.getSingleChannelBlock(1);

    juce::dsp::ProcessContextReplacing<float> mid_context(mid_block);
    juce::dsp::ProcessContextReplacing<float> side_context(side_block);

    _mid_chain.process(mid_context);
    _side_chain.process(side_context);

    LeftRightEncode(buffer, num_samples);
}

bool DelayVSTAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

juce::AudioProcessorEditor* DelayVSTAudioProcessor::createEditor()
{
    return new juce::GenericAudioProcessorEditor(*this);
}

void DelayVSTAudioProcessor::getStateInformation (juce::MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
    constexpr bool append_to_existing_block = true;
    juce::MemoryOutputStream stream(destData, append_to_existing_block);
    _processor_value_tree_state.state.writeToStream(stream);
}

void DelayVSTAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
    const auto tree = juce::ValueTree::readFromData(data, sizeInBytes);
    if (tree.isValid())
    {
        _processor_value_tree_state.replaceState(tree);
        updateFilters();
    }
}

juce::AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    // This creates new instances of the plugin..
    return new DelayVSTAudioProcessor();
}

DelayVSTAudioProcessor::MonoChainConfig DelayVSTAudioProcessor::createMidChainConfig(juce::AudioProcessorValueTreeState& value_tree_state)
{
    MonoChainConfig mid_config;

    DelayVST::LowpassFilterConfig& lowpass_config = std::get<(int)ChainPositions::Lowpass>(mid_config);
    lowpass_config._lowpass_freq_hz = value_tree_state.getRawParameterValue(DelayVST::MidChainParameters::lowpass_freq._name)->load();

    DelayVST::HighpassFilterConfig& highpass_config = std::get<(int)ChainPositions::Highpass>(mid_config);
    highpass_config._highpass_freq_hz = value_tree_state.getRawParameterValue(DelayVST::MidChainParameters::highpass_freq._name)->load();

    //auto& peak_1_config = config.at<DelayVST::ChainPositions::Peak_1>();
    //peak_1_config._peak_freq = value_tree_state.getRawParameterValue(peak_1_freq)->load();
    //peak_1_config._peak_gain_db = value_tree_state.getRawParameterValue(peak_1_gain)->load();
    //peak_1_config._peak_quality = value_tree_state.getRawParameterValue(peak_1_quality)->load();

    //auto& peak_2_config = config.at<DelayVST::ChainPositions::Peak_2>();
    //peak_2_config._peak_freq = value_tree_state.getRawParameterValue(peak_2_freq)->load();
    //peak_2_config._peak_gain_db = value_tree_state.getRawParameterValue(peak_2_gain)->load();
    //peak_2_config._peak_quality = value_tree_state.getRawParameterValue(peak_2_quality)->load();

    //auto& peak_3_config = config.at<DelayVST::ChainPositions::Peak_3>();
    //peak_3_config._peak_freq = value_tree_state.getRawParameterValue(peak_3_freq)->load();
    //peak_3_config._peak_gain_db = value_tree_state.getRawParameterValue(peak_3_gain)->load();
    //peak_3_config._peak_quality = value_tree_state.getRawParameterValue(peak_3_quality)->load();

     return mid_config;
}

DelayVSTAudioProcessor::MonoChainConfig DelayVSTAudioProcessor::createSideChainConfig(juce::AudioProcessorValueTreeState& value_tree_state)
{
    MonoChainConfig side_config;

    DelayVST::LowpassFilterConfig& lowpass_config = std::get<(int)ChainPositions::Lowpass>(side_config);
    lowpass_config._lowpass_freq_hz = value_tree_state.getRawParameterValue(DelayVST::SideChainParameters::lowpass_freq._name)->load();

    DelayVST::HighpassFilterConfig& highpass_config = std::get<(int)ChainPositions::Highpass>(side_config);
    highpass_config._highpass_freq_hz = value_tree_state.getRawParameterValue(DelayVST::SideChainParameters::highpass_freq._name)->load();

    return side_config;
}

juce::AudioProcessorValueTreeState::ParameterLayout DelayVSTAudioProcessor::createParameterLayout()
{
    juce::AudioProcessorValueTreeState::ParameterLayout layout;

    // add parameters for the mid chain
    DelayVST::MidChainParameters::lowpass_freq.addToLayout(layout);
    DelayVST::MidChainParameters::highpass_freq.addToLayout(layout);

    // add parameters for the side chain
    DelayVST::SideChainParameters::lowpass_freq.addToLayout(layout);
    DelayVST::SideChainParameters::highpass_freq.addToLayout(layout);

    return layout;
}

void DelayVSTAudioProcessor::updateFilters()
{
    const auto mid_chain_config = createMidChainConfig(_processor_value_tree_state);
    updateFilter<ChainPositions::Lowpass>(mid_chain_config, _mid_chain);
    updateFilter<ChainPositions::Highpass>(mid_chain_config, _mid_chain);

    const auto side_chain_config = createSideChainConfig(_processor_value_tree_state);
    updateFilter<ChainPositions::Lowpass>(side_chain_config, _side_chain);
    updateFilter<ChainPositions::Highpass>(side_chain_config, _side_chain);
}

DelayVSTAudioProcessor::FilterCoefficients DelayVSTAudioProcessor::createFilterCoefficients(const DelayVST::PeakFilterConfig& peak_filter_config)
{
    float gain_factor = juce::Decibels::decibelsToGain(peak_filter_config._peak_gain_db);

    // bad in multiple ways 
    //      - getSampleRate() is not guaranteed to be correct if called outside processBlock
    //      - we're also doing heap allocation on the audio thread
    auto peak_coefficients = juce::dsp::IIR::Coefficients<float>::makePeakFilter(
        getSampleRate(), peak_filter_config._peak_freq_hz, peak_filter_config._peak_quality, gain_factor);

    return peak_coefficients;
}

DelayVSTAudioProcessor::FilterCoefficients DelayVSTAudioProcessor::createFilterCoefficients(const DelayVST::LowpassFilterConfig& lowpass_filter_config)
{
    auto lowpass_coefficients = juce::dsp::IIR::Coefficients<float>::makeLowPass(
        getSampleRate(), lowpass_filter_config._lowpass_freq_hz);

    return lowpass_coefficients;
}

DelayVSTAudioProcessor::FilterCoefficients DelayVSTAudioProcessor::createFilterCoefficients(const DelayVST::HighpassFilterConfig& highpass_filter_config)
{
    auto highpass_coefficients = juce::dsp::IIR::Coefficients<float>::makeHighPass(
        getSampleRate(), highpass_filter_config._highpass_freq_hz);

    return highpass_coefficients;
}
