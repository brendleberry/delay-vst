/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#pragma once

#include "FilterConfigs.h"
#include <JuceHeader.h>

class DelayVSTAudioProcessor  : public juce::AudioProcessor
{
public:
    DelayVSTAudioProcessor();
    ~DelayVSTAudioProcessor() override;

    void prepareToPlay (double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

   #ifndef JucePlugin_PreferredChannelConfigurations
    bool isBusesLayoutSupported (const BusesLayout& layouts) const override;
   #endif

    void processBlock (juce::AudioBuffer<float>&, juce::MidiBuffer&) override;

    juce::AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    const juce::String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    bool isMidiEffect() const override;
    double getTailLengthSeconds() const override;

    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram (int index) override;
    const juce::String getProgramName (int index) override;
    void changeProgramName (int index, const juce::String& newName) override;

    void getStateInformation (juce::MemoryBlock& destData) override;
    void setStateInformation (const void* data, int sizeInBytes) override;

private:

    // Enum representing the position of the filters in the processing chain
    enum class ChainPositions : int
    {
        Lowpass = 0,
        Highpass = 1
    };

    using Filter = juce::dsp::IIR::Filter<float>;
    using FilterCoefficients = juce::dsp::IIR::Coefficients<float>::Ptr;

    using MonoChain = juce::dsp::ProcessorChain<Filter, Filter>;
    using MonoChainConfig = std::tuple<DelayVST::LowpassFilterConfig, DelayVST::HighpassFilterConfig>;

    /*!
        \brief Creates the chain config from a processor value tree state
        \param [in] value_tree_state The processor value tree state to create a chain config from
        \returns The created chain config
    */
    MonoChainConfig DelayVSTAudioProcessor::createMidChainConfig(juce::AudioProcessorValueTreeState& value_tree_state);
    MonoChainConfig DelayVSTAudioProcessor::createSideChainConfig(juce::AudioProcessorValueTreeState& value_tree_state);

    /*!
        \brief Creates the parameter layout, which is used to initialise the processor value tree state
    */
    static juce::AudioProcessorValueTreeState::ParameterLayout createParameterLayout();

    /*! 
        \brief Updates all of the filters
        \details Constructs the mid and side chain configs from the processor value tree state, 
            then uses these to update each filter in turn
    */
    void updateFilters();

    /*!
        \brief Updates the filter at a given position in a filter chain
        \tparam ChainPosition The position in the chain of the filter to update
        \param chain_config The config of the filter chain
        \param chain The filter chain to update
    */ 
    template<ChainPositions ChainPosition>
    void updateFilter(const MonoChainConfig& chain_config, MonoChain& chain);

    FilterCoefficients createFilterCoefficients(const DelayVST::PeakFilterConfig& peak_filter_config);
    FilterCoefficients createFilterCoefficients(const DelayVST::LowpassFilterConfig& lowpass_filter_config);
    FilterCoefficients createFilterCoefficients(const DelayVST::HighpassFilterConfig& highpass_filter_config);
    
    /*! The processing chains for the mid and side channels */
    MonoChain _mid_chain, _side_chain;
    /*! The processor value tree state, which manages all of the processor state */
    juce::AudioProcessorValueTreeState _processor_value_tree_state{ *this, nullptr, "Parameters", createParameterLayout() };

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (DelayVSTAudioProcessor)
};

template <DelayVSTAudioProcessor::ChainPositions ChainPosition> 
void DelayVSTAudioProcessor::updateFilter(const DelayVSTAudioProcessor::MonoChainConfig& chain_config, MonoChain& chain)
{
    const auto& filter_config = std::get<(int)ChainPosition>(chain_config);
    auto filter_coefficients = createFilterCoefficients(filter_config);
    *chain.get<(int)ChainPosition>().coefficients = *filter_coefficients;
}