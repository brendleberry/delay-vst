#pragma once

#include <boost/circular_buffer.hpp>
#include <vector>

namespace DelayVST
{
	class CircularAudioBuffer
	{
	public:

		CircularAudioBuffer() = default;

		CircularAudioBuffer(const int num_channels, const int buffer_length_samples)
			: _channel_buffers(num_channels, boost::circular_buffer<float>(buffer_length_samples))
		{
		}

		boost::circular_buffer<float>& operator[] (const int channel_index) 
		{ 
			return _channel_buffers[channel_index]; 
		}

	private:
		std::vector<boost::circular_buffer<float>> _channel_buffers;
	};
}